# Mini Projet II: Casse Briques 
L'objectif de ce mini projet est de créer une version simplifiée d'un jeu auquel vous avez probablement joué durant votre enfance : "Casse Briques ou Atari Breakout". Il faut faire contrôle une sorte de raquette qu'il peut seulement déplacer sur un axe horizontal au bas de l'écran, et le but est d'empêcher la balle de franchir cette ligne en l'interceptant avec la raquette.

Voici une image qui vous aidera certainement à mieux comprendre de quoi je parle :

![screenshot](cassebr.jpeg)


Les fichiers de code du mini-projet sont disponibles à l'adresse suivante : 

<https://gitlab.com/ssalhi/casse_briques.git>

## Début du jeu:

Au début du jeu, le champ est rempli de blocs.

La barre qui permet de diriger la balle se trouve dans la partie inférieure du champ de jeu.

Gardez toujours un oeil sur la balle, car si vous ratez la balle avec la raquette, la partie est perdue.

## Fonctionnement

Vous pouvez déplacer la barre vers la gauche ou vers la droite à l’aide des touches "r" et "t" du clavier.

L’angle de vol de la balle dépend du point de contact avec la raquette.

## Blocs

 Le jeu comporte 10 blocs de briques, auxquels chacune est attribuée à un 1 point de score.

## Fin de jeu

La fin de partie est déclaré soit quand :

Vous atteignez le score 10 lorsque vous avez rassemblé tous les briques.

Votre balle touche le sol sans avoir la collision avec la raquette.

## Schéma d'interraction
![screenshot](schemainterr.png)

